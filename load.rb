require 'digest'
require 'fileutils'
require 'json'
require 'open-uri'
require 'optparse'
require 'cgi'
require 'net/http'
# require 'dotenv/load'
require_relative './client/client'
require_relative './client/secure_file'

# configure the script from the CI environment variables
api_v4_url     = ENV['CI_API_V4_URL'] || 'https://gitlab.com/api/v4'
project_id     = ENV['CI_PROJECT_ID']
job_token      = ENV['CI_JOB_TOKEN']
private_token  = ENV['PRIVATE_TOKEN']
download_path  = ENV['SECURE_FILES_DOWNLOAD_PATH'] || '.secure_files'

# exit if required environment variables are missing
raise "CI_API_V4_URL undefined" if api_v4_url.nil?
raise "CI_PROJECT_ID undefined" if project_id.nil?
raise "Access Token undefined, define either CI_JOB_TOKEN or PRIVATE_TOKEN" if job_token.nil? && private_token.nil?

# setup client
$gitlab_client = GitLab::Client.new(
  job_token: job_token, 
  private_token: private_token, 
  project_id: project_id, 
  api_v4_url: api_v4_url,
  download_path: download_path
)

def fetch_secure_files
  $gitlab_client.files.each do |secure_file|
    secure_file.download
  end
end

fetch_secure_files
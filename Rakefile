# For Bundler.with_clean_env
require 'bundler/setup'

PACKAGE_NAME = "load-secure-files"
VERSION = "1.0.0"
TRAVELING_RUBY_VERSION = "20210206-2.4.10"

desc "Package your app"
task :package => ['package:windows', 'package:linux', 'package:osx']

namespace :package do
  desc "Package your app for x86_64-win32"
  task :windows => [:bundle_install, "packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-x86_64-win32.tar.gz"] do
    create_package("x86_64-win32")
    cleanup("x86_64-win32")
  end

  desc "Package your app for Linux x86_64"
  task :linux => [:bundle_install, "packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-linux-x86_64.tar.gz"] do
    create_package("linux-x86_64")
    cleanup("linux-x86_64")
  end

  desc "Package your app for OS X"
  task :osx => [:bundle_install, "packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-osx.tar.gz"] do
    create_package("osx")
    cleanup("osx")
  end

  desc "Install gems to local directory"
  task :bundle_install do
    sh "rm -rf packaging/tmp"
    sh "mkdir packaging/tmp"
    sh "cp Gemfile Gemfile.lock packaging/tmp/"
    Bundler.with_clean_env do
      sh "cd packaging/tmp && env BUNDLE_IGNORE_CONFIG=1 bundle install --path ../vendor --without development"
    end
    sh "rm -rf packaging/tmp"
    sh "rm -f packaging/vendor/*/*/cache/*"
  end
end

file "packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-x86_64-win32.tar.gz" do
  download_runtime("x86_64-win32")
end

file "packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-linux-x86_64.tar.gz" do
  download_runtime("linux-x86_64")
end

file "packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-osx.tar.gz" do
  download_runtime("osx")
end

def create_package(target)
  package_dir = "#{PACKAGE_NAME}-#{target}"
  sh "rm -rf #{package_dir}"
  sh "mkdir #{package_dir}"
  sh "mkdir -p #{package_dir}/lib/app"
  sh "cp load.rb #{package_dir}/lib/app/"
  sh "cp -r client #{package_dir}/lib/app/"
  sh "mkdir #{package_dir}/lib/ruby"
  sh "tar -xzf packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-#{target}.tar.gz -C #{package_dir}/lib/ruby"
  sh "cp packaging/wrapper.sh #{package_dir}/load"
  sh "chmod +x #{package_dir}/load"
  sh "cp -pR packaging/vendor #{package_dir}/lib/"
  sh "cp Gemfile Gemfile.lock #{package_dir}/lib/vendor/"
  sh "mkdir #{package_dir}/lib/vendor/.bundle"
  sh "cp packaging/bundler-config #{package_dir}/lib/vendor/.bundle/config"
  sh "tar -czf releases/#{package_dir}.tar.gz #{package_dir}"
  sh "rm -rf #{package_dir}"
end

def cleanup(target)
  system "rm packaging/traveling-ruby-#{TRAVELING_RUBY_VERSION}-#{target}.tar.gz"
end

def download_runtime(target)
  sh "cd packaging && curl -L -O --fail " +
    "http://d6r77u77i8pq3.cloudfront.net/releases/traveling-ruby-#{TRAVELING_RUBY_VERSION}-#{target}.tar.gz"
end